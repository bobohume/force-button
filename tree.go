package main

import "fmt"

type(
	Tree struct {
		left *Tree
		right *Tree
		data int
	}
)

func BuildTree(tree *Tree, data int) *Tree{
	if tree == nil{
		tree = &Tree{data:data}
		tree.left, tree.right = nil, nil
		tree.data = data
		return tree
	}

	if  tree.data < data{
		tree = BuildTree(tree.left, data)
	}else if tree.data > data{
		tree = BuildTree(tree.right, data)
	}
	return tree
}

func main(){
	head := BuildTree(nil, 2)
	tree := BuildTree(head, 3)
	tree = BuildTree(tree, 1)

	tree = BuildTree(tree, 4)
	fmt.Println(head)
}