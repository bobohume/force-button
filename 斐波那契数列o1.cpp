#include <iostream>

template<int T>
struct Fabi
{
    enum
    {
        Result = Fabi<T-1>::Result + Fabi<T-2>::Result
    };
};

template<>
struct Fabi<1>
{
    enum
    {
        Result = 1
    };
};

template<>
struct Fabi<2>
{
    enum
    {
        Result = 1
    };
};

constexpr int fabi(int n)
{
    if(n < 2)
        return n;
    return fabi(n-1) + fabi(n-2);
}

int main()
{
    return Fabi<10>::Result + fabi(10);
}