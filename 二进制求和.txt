// ConsoleApplication2.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <vector>
#include <deque>
#include <stack>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <xfunctional>
#include <queue>
using namespace std;

/*
给定两个二进制字符串，返回他们的和（用二进制表示）。
输入为非空字符串且只包含数字 1 和 0。
示例:
示例 1:
输入: a = "11", b = "1"
输出: "100"

示例 2:
输入: a = "1010", b = "1011"
输出: "10101"
*/
std::string add(string a, string b)
{
	std::string c;
	bool bFlag = false;
	reverse(a.begin(), a.end());
	reverse(b.begin(), b.end());
	int iLen = min(a.length(), b.length());
	int iMax = max(a.length(), b.length());
	c.resize(iMax + 1);
	for (int i = 0; i < iLen; i++)
	{
		int cSum = a[i] - '0' + b[i] - '0';
		if (bFlag)
		{
			cSum++;
		}
		c[i] = (cSum) % 2 + '0';
		bFlag = (cSum) / 2;
	}

	for (int i = iLen; i < a.length(); i++)
	{
		int cSum = a[i] + 0 - '0';
		if (bFlag)
		{
			cSum++;
		}
		c[i] = (cSum) % 2 + '0';
		bFlag = (cSum) / 2;
	}

	for (int i = iLen; i < b.length(); i++)
	{
		int cSum = b[i] + 0 - '0';
		if (bFlag)
		{
			cSum++;
		}
		c[i] = (cSum) % 2 + '0';
		bFlag = (cSum) / 2;
	}

	if (bFlag)
	{
		c[iMax] = '1';
	}
	return c;
}

int _tmain(int argc, _TCHAR* argv[])
{	
	int kkk1 = 0x12345678;
	string ccc = add("1010", "1011");
	int kkk = 0;
}