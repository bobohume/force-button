#include <iostream>
#include <cmath>
#include<vector>
#include<list>
using namespace std;

class Base
{
public:
    int a = 1;
    virtual void print(int n=2)
    {
        printf("Base%d\n", a+n);
    };
};

class Dervie:public Base
{
public:
    int b = 3;
    virtual void print(int n=10)
    {
        printf("Dervie_%d\n", b+n);
    };
};

int main()
{
    Base* ptr = new Dervie[10];
    ptr[1].print();
    return 0;
}