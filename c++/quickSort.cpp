// ConsoleApplication1.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <vector>
#include <iostream>

void QuickSort(std::vector<int>& numsVec, int left, int right)
{
	auto i = left, j = right;
	if (i >= j)
	{
		return;
	}

	auto key = numsVec[i];
	for (; i < j;)
	{
		for (; i < j; j--)
		{
			if (numsVec[j] < key)
			{
				break;
			}
		}
		numsVec[i] = numsVec[j];
		for (; i < j; i++)
		{
			if (numsVec[i] > key)
			{
				break;
			}
		}
		numsVec[j] = numsVec[i];
	}
	numsVec[i] = key;
	QuickSort(numsVec, left, i);
	QuickSort(numsVec, i + 1, right);
}

int _tmain(int argc, _TCHAR* argv[])
{
	std::vector<int> vec{ 0, 9, 8, 3, 2 };
	QuickSort(vec, 0, 4);
	auto kkk = 0;
	return 0;
}

