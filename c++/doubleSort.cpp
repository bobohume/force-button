// ConsoleApplication1.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <vector>
#include <iostream>

int Search(std::vector<int>& numsVec, int val)
{
	int l = 0, h = numsVec.size() - 1;
	for (; l <= h;)
	{
		int mid = (l + h) >> 1;
		int key = numsVec[mid];
		if (numsVec[mid] > val)
		{
			h = mid - 1;
		}
		else if (numsVec[mid] < val)
		{
			l = mid + 1;
		}
		else
		{
			return mid;
		}
	}
	return -1;
}

int _tmain(int argc, _TCHAR* argv[])
{
	std::vector<int> vec{ 0, 2, 5, 7, 9, 10 };
	auto kkkk1  = Search(vec, 5);
	auto kkk = 0;
	return 0;
}

