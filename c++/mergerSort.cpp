// ConsoleApplication1.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <vector>
#include <iostream>

std::vector<int> megerSort(std::vector<int> Nums0, std::vector<int> Nums1)
{
	std::vector<int> res;
	auto iLen0 = Nums0.size(), iLen1 = Nums1.size();
	auto i0 = 0, i1 = 0;
	for (; i0 < iLen0 || i1 < iLen1;){
		if (i0 == iLen0)
		{
			res.push_back(Nums1[i1++]);
		}
		else if (i1 == iLen1)
		{
			res.push_back(Nums0[i0++]);
		}
		else if (Nums0[i0] < Nums1[i1])
		{
			res.push_back(Nums0[i0++]);
		}
		else
		{
			res.push_back(Nums1[i1++]);
		}
	}
	return  res;
}

std::vector<int> merger(std::vector<int> Nums)
{
	if (Nums.size() < 2)
	{
		return Nums;
	}
	auto mid = Nums.size() >> 1;
	std::vector<int> left;
	std::vector<int> right;
	for (auto i = 0; i < mid; i++)
	{
		left.push_back(Nums[i]);
	}
	for (auto i = mid; i < Nums.size(); i++)
	{
		right.push_back(Nums[i]);
	}
	return megerSort(merger(left), merger(right));
}

int _tmain(int argc, _TCHAR* argv[])
{
	auto jjj = merger(std::vector<int>{0, 9, 8, 3, 2});
	auto kkk = 0;
	return 0;
}

