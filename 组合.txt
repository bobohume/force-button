// test.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <vector>
#include <deque>
#include <stack>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <queue>
using namespace std;

/*
给定两个整数 n 和 k，返回 1 ... n 中所有可能的 k 个数的组合。
示例:
输入: n = 4, k = 2
输出:
[
  [2,4],
  [3,4],
  [2,3],
  [1,2],
  [1,3],
  [1,4],
]
*/

void dfs(int i, int n, int k, std::vector<int> vec, std::vector<std::vector<int>>& res)
{
	if (vec.size() == k)
	{
		res.push_back(vec);
		return;
	}

	if (i > n)
	{
		return;
	}

	for (int j = i; j < n; j++)
	{
		vec.push_back(j);
		dfs(j + 1, n, k, vec, res);
		vec.pop_back();
	}
}

int main()
{
	std::vector<int> vec;
	std::vector<std::vector<int>> res;
	dfs(1, 5, 2, vec, res);
    std::cout << "Hello World!\n";
}