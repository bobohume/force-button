package main

import "fmt"

//题目描述:
//设计一个支持 push，pop，top 操作，并能在常数时间内检索到最小元素的栈。
//push(x) -- 将元素 x 推入栈中。
//pop() -- 删除栈顶的元素。
//top() -- 获取栈顶元素。
//getMin() -- 检索栈中的最小元素。
//MinStack minStack = new MinStack();
//minStack.push(-2);
//minStack.push(0);
//minStack.push(-3);
//minStack.getMin();   --> 返回 -3.
//minStack.pop();
//minStack.top();      --> 返回 0.
//minStack.getMin();   --> 返回 -2.
//思路:
//思路一:使用两个栈
//一个栈记录 压入元素
//一个栈记录 最小元素
//思路二 : 使用一个栈[1]
//当有更小元素进入栈中, 把先前最小值压入栈中
type (
	MinStack struct {
		m_Array	[10]int
		m_ArraySize int

		m_MinArray [10]int
		m_MinSize int
	}
)

func (this *MinStack) push(x int){
	if this.m_MinSize == 0{
		this.m_MinArray[this.m_MinSize] = x
	}else if this.m_MinArray[this.m_MinSize-1] > x{
		this.m_MinArray[this.m_MinSize] = x
	}else{
		this.m_MinArray[this.m_MinSize] = this.m_MinArray[this.m_MinSize-1]
	}
	this.m_Array[this.m_ArraySize] = x
	this.m_ArraySize++
	this.m_MinSize++
}

func (this *MinStack) pop(){
	if this.m_ArraySize > 0{
		this.m_ArraySize--
		this.m_MinSize--
	}
}

func (this *MinStack) top()int{
	if this.m_ArraySize > 0 {
		return this.m_Array[this.m_ArraySize-1]
	}
	return -1
}

func (this *MinStack) getMin()int{
	if this.m_MinSize > 0 {
		return this.m_MinArray[this.m_MinSize-1]
	}
	return -1
}

func main(){
	minStack := MinStack{};
	minStack.push(-2)
	minStack.push(0)
	minStack.push(-3)
	fmt.Println(minStack.getMin())
	minStack.pop()
	fmt.Println(minStack.top())
	fmt.Println(minStack.getMin())
}