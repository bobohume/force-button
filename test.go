package main

import "fmt"

func subarray(num []int, pos int, res []int)[]int{
	//if pos >= len(num){
	//	return []int{}
	//}

	for i := pos; i < len(num); i++{
		res = append(res, num[i])
		subarray(num, i+1, res)
		res = append(res[:len(res)-1], res[len(res):]...)
	}
	fmt.Println(res)

	return res
}

func main(){
	subarray([]int{1, 2, 3}, 0, []int{})
}
