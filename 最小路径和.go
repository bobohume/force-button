package main

import (
	"fmt"
	"math"
)

//给定一个包含非负整数的 m x n 网格，请找出一条从左上角到右下角的路径，使得路径上的数字总和为最小。
//说明：每次只能向下或者向右移动一步。
// 输入:
// [
//   [1,3,1],
//   [1,5,1],
//   [4,2,1]
// ]
// 输出: 7
// 解释: 因为路径 1→3→1→1→1 的总和最小。

func deepmin(num [][]int) {
	dp := make([][]int, len(num))
	for i := 0; i < len(num); i++{
		dp[i] = make([]int, len(num))
	}

	dp[0][0] = num[0][0]
	for i := 1; i < len(num); i++ {
		dp[i][0] =  dp[i-1][0] + num[i][0]
	}

	for j := 1; j < len(num); j++ {
		dp[0][j] =  dp[0][j-1] + num[0][j]
	}

	for i := 1; i < len(num); i++{
		for j := 1; j < len(num); j++{
			dp[i][j] =  int(math.Min(float64(dp[i][j-1]),float64( dp[i-1][j]))) + num[i][j]
		}
	}

	fmt.Println(dp)
}

func main(){
	deepmin([][]int{
		{1, 3, 1},
		{1, 5, 1},
		{4, 2, 1},
	})
}
