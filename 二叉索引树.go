package main

import "fmt"

type(
	TreeNode struct {
		data int
		left *TreeNode
		right *TreeNode
	}
)

//题目：
//给定一个整数 n，生成所有由 1 … n 为节点所组成的二叉搜索树。
//示例:
//输入: 3
//输出:
//[
//  [1,null,3,2],
//  [3,2,null,1],
//  [3,1,null,null,2],
//  [2,1,3],
//  [1,null,2,null,3]
//]
//解释:
//以上的输出对应以下 5 种不同结构的二叉搜索树：
//
//   1         3     3      2      1
//    \       /     /      / \      \
//     3     2     1      1   3      2
//    /     /       \                 \
//   2     1         2                 3
func generateTree(start, end int) []*TreeNode{
	allTree := []*TreeNode{}
	if start > end{
		allTree = append(allTree, nil)
		return allTree
	}

	for i := start; i <= end; i++{
		left := generateTree(start, i-1)
		right := generateTree(i+1, end)
		for _, v := range left{
			for _, v1 := range right{
				data := TreeNode{data:i}
				data.left = v
				data.right = v1
				allTree = append(allTree, &data)
			}
		}
	}

	for _,	v := range allTree{
		fmt.Print(v)
	}


	fmt.Println("----")
	return allTree
}

func main(){
	generateTree(1, 3)
}