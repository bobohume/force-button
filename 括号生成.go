package main

import "fmt"

const(
	Split_0 = '('
	Split_1 = ')'
)

//给定一个只包含 '(' 和 ')' 的字符串，找出最长的包含有效括号的子串的长度。
//
//示例 1:
//输入: "(()"
//输出: 2
//解释: 最长有效括号子串为 "()"

//示例 2:
//
//输入: ")()())"
//输出: 4
//解释: 最长有效括号子串为 "()()"

func generateAll(n int, str []byte, pos int){
	if pos == n{
		if vail(str){
			fmt.Println(string(str))
		}
		return
	}

	str[pos] = Split_0;
	generateAll(n, str, pos+1)

	str[pos] = Split_1 ;
	generateAll(n, str, pos+1)
}

func vail(str []byte) bool{
	num := 0
	for i := 0; i < len(str); i++{
		if str[i] == Split_0{
			num++
		}else{
			num--
		}

		if num < 0{
			return false
		}
	}
	return num  == 0
}

func main(){
	n := 5
	str := make([]byte, n)
	generateAll(n ,str, 0)
	fmt.Println(str)
}