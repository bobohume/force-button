package main

import "fmt"

//统计所有小于非负整数 n 的质数的数量。
//示例:
//输入: 10
//输出: 4
//解释: 小于 10 的质数一共有 4 个, 它们是 2, 3, 5, 7 。
func getPrime(n int){
	for i := 2; i < n; i++{
		bPrime := true
		for j := 2; j < i; j++{
			if i % j == 0 {
				bPrime = false
				break
			}
		}
		if bPrime{
			fmt.Println(i)
		}
	}
}

func main(){
	getPrime(10)
}