package main

import (
	"fmt"
)

func quicksort(arr []byte, left, right int){
	if left >= right{
		return
	}

	i, j := left, right
	key := arr[i]
	for i < j{
		for ;i < j; j--{
			if arr[j] < key{
				break
			}
		}
		arr[i] = arr[j]

		for ; i < j; i++{
			if arr[i] > key{
				break
			}
		}
		arr[j] = arr[i]
	}
	arr[i] = key
	quicksort(arr, left, i)
	quicksort(arr, i+1, right)
}

func dictsort(dics []byte){
	i, j := 0, 0
	for{
		if i < 0  && j < 0{
			break
		}
		for i = len(dics) -2; i >= 0; i--{
			if dics[i+1] > dics[i]{
				for j = len(dics) -1; j > i; j--{
					if dics[j] > dics[i]{
						temp := dics[j]
						dics[j] = dics[i]
						dics[i] = temp
						quicksort(dics[i+1:], 0, len(dics[i+1:]) -1 )
						fmt.Println(dics)
						break
					}
				}
			}
		}
	}

}

func main(){
	//dictsort([]byte{1, 2, 3})
	dictsort([]byte{1, 2, 4, 6, 5, 3})
}