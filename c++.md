### auto
    auto expr
    - expr包含cv描述符, const int a = 1; auto b = a; 此时b为int，会丢失const
    - const int a[10]; auto b = a; b退化为int* [列子](https://godbolt.org/z/GEvh1vbes)
    对比
    - const int a = 1; auto& b = a;此时b为const int &
    - int a[10]; auto& b = a;此时b为int(&)[10]
    
    auto函数返回值推到类型
    template<class T， class U>
    ? add(T t,U u) {return a + b}
    可以这样
    template<class T， class U>
    auto add(T t,U u) {return a + b} auto类型为operator+(T,U)

    普通函数
    auto equaltOne(int x){
        if(x==1)
            return true;
        else
            return false;
    }
    编译器要求如果一个函数有多个return语句的时候，需要推倒成相同的类型
    否则会类似error: inconsistent deduction for auto return type: 'bool' and then 'int'

    在new表达式
    auto p = new auto('c') // p is a char*

    
### decltype
    - decltype(expr)返回一个表达式的类型
    - example1
    std::map<char,int> mymap;
    mymap.insert(std::pair<char, int>('a', 100));
    mymap.insert(std::pair<char, int>::value_type('b', 101));
    mymap.insert(decltype(mymap)::value_type('c', 102));
    - example2
    std::map<int,std::string> ismap;
    std::map<std::string,int> simap;
    通过上述的map来获取下面的map，颠倒key和value的类型
    std::map<typename decltype(ismap)::mapped_type, typename decltype(ismap)::key_type> simap;
    也可以
    template<typename MapType>
    auto RevertMap(MapType ismap){
        - return std::map<typename decltype(ismap)::mapped_type, typename decltype(ismap)::key_type>();
        - return std::map<typename MapType::mapped_type, typename MapType ::key_type>();
    }
    
    - decltype(expr) 返回一个表达式的类型
    int x = 1;
    decltype((x)); //int &
    
    //函数返回
    struct A{};
    A& func();
    decltype(func()); //A&

    // ++
    int x = 1; int y = 2;
    decltype(++x); //int& x的值并不会增加，因为编译器推倒的，而不是真的去执行
    decltype(x+y);
    注意 decltype(expr) 表达式并不会真的会执行，因为编译器推倒类型的，并不会真的被执行、
     