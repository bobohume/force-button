package main

import "fmt"

//快排
func quickSort(nums []int, left, right int){
	i, j := left, right
	if i >= j{
		return
	}
	key := nums[i]

	for i < j{
		for ; i <j; j--{
			if nums[j] < key{
				break
			}
		}
		nums[i] = nums[j]
		for ; i <j; i++{
			if nums[i] > key{
				break
			}
		}
		nums[j] = nums[i]
	}
	nums[i] = key
	quickSort(nums, left, i)
	quickSort(nums, i+1, right)
}

func main(){
	nums := []int{9, 10, 3, 4, 5, 9}
	quickSort(nums, 0, len(nums)-1)
	fmt.Println(nums)
}