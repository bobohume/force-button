package main

import "fmt"

/*
给定一个没有重复数字的序列，返回其所有可能的全排列。
示例:
输入: [1,2,3]
输出:
[
  [1,2,3],
  [1,3,2],
  [2,1,3],
  [2,3,1],
  [3,1,2],
  [3,2,1]
]
*/

func swap(nums []int, i, j int){
	temp := nums[i]
	nums[i] = nums[j]
	nums[j] = temp
}

func allsort(nums []int, pos int){
	if pos >= len(nums)-1{
		fmt.Println(nums)
		return
	}

	for i := pos; i <= len(nums) - 1; i++{
		swap(nums, i, pos)
		allsort(nums, pos+1)
		swap(nums, pos, i)
	}
}

func main(){
	allsort([]int{2, 3, 5},0)
}
