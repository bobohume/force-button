# go

[gmp](https://zhuanlan.zhihu.com/p/261590663)[1](https://studygolang.com/articles/27934?fr=sidebar)

1.由于原子、互斥量或channel操作调用导致阻塞，调度器将把当前阻塞的 Goroutine 切换出去，重新调度 LRQ 上的其他 Goroutine。

2.由于网络请求和 IO 操作导致 Goroutine

Go 程序提供了网络轮询器（NetPoller）来处理网络请求和 IO 操作的问题，其后台通过 kqueue（MacOS），epoll（Linux）或 iocp（Windows）来实现 IO 多路复用。

通过使用 NetPoller 进行网络系统调用，调度器可以防止 Goroutine 在进行这些系统调用时阻塞 M。这可以让 M 执行 P 的 LRQ 中其他的 Goroutines，而不需要创建新的 M。有助于减少操作系统上的调度负载。

G1 正在 M 上执行，还有 3 个 Goroutine 在 LRQ 上等待执行。网络轮询器空闲着，什么都没干

3.当调用一些系统方法的时候，如果系统方法调用的时候发生阻塞，这种情况下，网络轮询器（NetPoller）无法使用，而进行系统调用的 Goroutine 将阻塞当前 M。

4.在Goroutine中去执行一个sleep操作，导致M被阻塞

5.[如果一个系统调用或者G任务执行太长，他就会一直占用这个线程，由于本地队列的G任务是顺序执行的，其它G任务就会阻塞了，怎样中止长任务的呢]
(https://www.cfanz.cn/mobile/resource/detail/JpGylogvDpngn)

这样滴，启动的时候，会专门创建一个线程sysmon，用来监控和管理，在内部是一个循环：

1.记录所有P的G任务计数schedtick，（schedtick会在每执行一个G任务后递增）

2.如果检查到 schedtick一直没有递增，说明这个P一直在执行同一个G任务，如果超过一定的时间（10ms），就在这个G任务的栈信息里面加一个标记

3.然后这个G任务在执行的时候，如果遇到非内联函数调用，就会检查一次这个标记，然后中断自己，把自己加到队列末尾，执行下一个G

4.如果没有遇到非内联函数（有时候正常的小函数会被优化成内联函数）调用的话，那就惨了，会一直执行这个G任务，直到它自己结束；如果是个死循环，并且GOMAXPROCS=1的话，恭喜你，夯住了！亲测，的确如此

对于一个G任务，中断后的恢复过程：

1.中断的时候将寄存器里的栈信息，保存到自己的G对象里面

2.当再次轮到自己执行时，将自己保存的栈信息复制到寄存器里面，这样就接着上次之后运行了。 


[gc](https://zhuanlan.zhihu.com/p/92210761)

[stl github](https://github.com/emirpasic/gods)

# redis

[穿透击穿雪崩](https://www.cnblogs.com/xichji/p/11286443.html)

# c++

[shared_ptr](https://blog.csdn.net/u012411498/article/details/80845705)

[RTTI](http://www.openrce.org/articles/full_view/23)

# tcp 拥塞控制

慢开始:简单的说,开始传输时,传输的数据由小到大递增到一个值(即发送窗口由小到大(指数增长)逐渐增大到拥塞窗口的数值).

拥塞避免:数据发送出去,并发到接收方发回来的确认收到,拥塞窗口每次值加1地线性增大.

快重传:数据传输时接手方收到发送方3个ack确定发送丢失前的数据的重复确认,这样发送方就知道有部分数据丢失了,于是从丢失出重传数据.3个ack

快恢复:快恢复是与快重传配合的算法,在发生数据丢失时,发送方收到接收方发回的三个重复确认信息时,就把每次传输的数据量减为原来的一半,拥塞窗口也修改为这个值,然后又开始拥塞避免的算法.  

# TOP K

排序，大小堆（优先队列），分支分成n组排序k个，再进行大小堆排序，hash以后再排序


#  
2.指向常量的指针

const ——int*p

指针所保存的地址可以改变，然而指针所指向的值却不可以改变。同理，当添加*p = b时，会发生编译错误！

3.常指针

int const*p

特点是指针所保存的地址不可变，指针所指向的数值也不可变


## 操作系统

# 虚拟内存 

虚拟内存作用防止不同进程同一时刻对物理内存的竞争和践踏，采用虚拟内存，是不同进程独占自己4g内存，

所有进程共享物理内存，每个进程把自己的虚拟内存映射并存储到物理内存，不是立马拷贝，而是采用缺页异常在拷贝

虚拟内存好处:扩大地址空间，内存保护，公平分配，虚拟内存连续，不需要物理内存

# 死锁

死锁条件：互斥条件，保持和等待，不可剥夺，环路等待。

解决：不可剥夺，一次分配，(有序分配)按序列号自增，释放反之

#[虚函数](https://blog.csdn.net/primeprime/article/details/80776625) 


# 浮点数
float  符号1 指数 8 尾数 23   10进制有效位数 6-7   double 符号1 指数11 尾数52 10进制有效为数15-16


## [mysql](https://zhuanlan.zhihu.com/p/164519371)

#事务

1.原子性:要么全部执行成功,要么失败。		2一致性:事务没提交之前,更新的数据不生效。		3.隔离性:事务a和事务b互不影响。			

4.永久性:事务一旦提交对数据永久保存。

#隔离级别

1.读未提交:Read uncommitted:就是一个事务可以读取另一个未提交事务的数据。

2.读提交:就是一个事务要等另一个事务提交后才能读取数据。

3.可重读(默认):就是在开始读取数据（事务开启）时，不再允许修改操作。MySQL的默认事务隔离级别（幻读：
	
	读取超出范围，事务b新加了一行，就会出现幻读）

4.Serializable（可串行化）:是在每个读的数据行上加上共享锁。在这个级别，可能导致大量的超时现象和锁竞争。

## 分布式事务

#[分布式事务](https://www.cnblogs.com/savorboard/p/distributed-system-transaction-consistency.html?ivk_sa=1024320u)

2PC

TCC(补偿事务 Try, Confirm, Cancel)

本地消息表（异步确保）

MQ 事务消息

## reids
# [类型](https://www.jianshu.com/p/623c6a862780)
# HASH 为什么是2的次幂. （n-1）& key. 添加元素分布更加均匀，减少hash冲突,提高查询效率

## mtu

最佳大小996
# [mtu](https://www.toutiao.com/i7042481774177403396/?tt_from=copy_link&utm_campaign=client_share&timestamp=1639840186&app=news_article_lite&utm_source=copy_link&utm_medium=toutiao_android&use_new_style=1&req_id=202112182309460101980670361DCD6491&share_token=e7067de3-41e9-40d1-96fb-faa0a57db588&group_id=7042481774177403396)

# 抽奖![输入图片说明](c++/image.png)

## snowfalke

ntp时钟回拨问题

## etcd

Etcd在服务器上读性能很优秀，单节点和集群线性读性能均在50000QPS以上，非线性读性能在70000QPS以上
单节点ETCD写性能优秀，能达到30000QPS，在不同时间点甚至达到45000左右。
但是由于一致性算法的制约，集群写性能超过7000QPS。就有可能报错 etcdserver: too many requests。
# Linearizable 读取请求
benchmark --endpoints=${HOST_1},${HOST_2},${HOST_3} --conns=1 --clients=1 range foo --consistency=l --total=10000
benchmark --endpoints=${HOST_1},${HOST_2},${HOST_3} --conns=100 --clients=1000 range foo --consistency=l --total=100000

# Serializable 读取请求，使用每个成员然后将数字加起来
for endpoint in ${HOST_1} ${HOST_2} ${HOST_3}; do  
    benchmark --endpoints=$endpoint --conns=1 --clients=1 range foo --consistency=s --total=10000

## mmo aoi优化
aoi，减少同步次数 和 降频；ai事件过滤；子弹就是flag；

# 性能测试
采样 pprof, pprofile说话，火焰图

插桩hook，比如-pg 在函数开始假如时间戳，结束得时候算时间戳差值

# [mesi协议](https://blog.csdn.net/xiaowenmu1/article/details/89705740)

# hotfix

找到a()和b()机器码所在的内存地址

构造跳转指令

修改a()函数的机器码

# 现在游戏引擎分层

5平台层  4核心层  内存管理容器数学管理   3资源层 2功能层  1编辑器