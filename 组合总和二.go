package main

import "fmt"

/*给定一个数组 candidates 和一个目标数 target ，找出 candidates 中所有可以使数字和为 target 的组合。
candidates 中的每个数字在每个组合中只能使用一次。
说明：
所有数字（包括目标数）都是正整数。
解集不能包含重复的组合。
示例 1:
输入: candidates = [10,1,2,7,6,1,5], target = 8,
所求解集为:
[
[1, 7],
[1, 2, 5],
[2, 6],
[1, 1, 6]
]
示例 2:

输入: candidates = [2,5,2,1,2], target = 5,
所求解集为:
[
[1,2,2],
[5]
]*/
func combineNums(nums []int, res []int, target int, pos int){
	if target < 0{
		return
	}else if target == 0{
		fmt.Println(res)
		return
	}


	for i := pos; i < len(nums); i++{
		if len(res) > 0 && res[len(res)-1] == nums[i]{
			continue
		}

		res = append(res, nums[i])
		combineNums(nums, res, target - nums[i], i+1)
		res = append(res[:len(res)-1], res[len(res):]...)
	}
}

func main(){
	res := []int{}
	combineNums([]int{2,5,2,1,2}, res,5, 0)
}