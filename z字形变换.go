package main

import (
	"fmt"
	"math"
)
//将一个给定字符串根据给定的行数，以从上往下、从左到右进行 Z 字形排列。
//比如输入字符串为 “LEETCODEISHIRING” 行数为 3 时，排列如下
//L   C   I   R
//E T O E S I I G
//E   D   H   N
func min(a, b int) int{
	return int(math.Min(float64(a), float64(b)))
}

//Z字形
func convert(s string, numRows int)string{
	row := min(len(s), numRows-1)
	rows := []string{}
	for i := 0; i <= row; i++{
		rows = append(rows, "")
	}
	nRow := 0
	grow := false
	for i, _ := range s{
		rows[nRow] += string(s[i])
		if nRow == 0 || nRow == row{
			grow = !grow
		}
		if grow {
			nRow++
		}else{
			nRow--
		}
	}

	out := ""
	for _, v := range rows{
		out += v
	}
	return out
}


func main(){
	fmt.Println(convert("LEETCODEISHIRING", 3))
}