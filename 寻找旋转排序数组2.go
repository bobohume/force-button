package main

import "fmt"

//题目：
//示例 1：
//输入: [1,3,5]
//输出: 1
//示例 2：
//输入: [2,2,2,0,1]
//输出: 0
//说明：
//这道题是 寻找旋转排序数组中的最小值 的延伸题目。
//允许重复会影响算法的时间复杂度吗？会如何影响，为什么？
//分析：
//这题没有复杂度优于O（n）的解法

func findMin(arr []int) int{
	l, h := 0, len(arr)-1
	for l < h{
		mid := (l+h)>>1
		if arr[mid] > arr[h]{
			l = mid + 1
		}else if arr[mid] < arr[h]{
			h = mid
		}else{
			h  = h - 1
		}
	}
	return arr[l]
}

func main(){
	fmt.Println(findMin([]int{2,2,2,2,0,1}))
}
