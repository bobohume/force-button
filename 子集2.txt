// ConsoleApplication2.cpp : 定义控制台应用程序的入口点。
// ConsoleApplication2.cpp : 定义控制台应用程序的入口点。
//

#include <vector>
#include <deque>
#include <stack>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <queue>
using namespace std;

/*
给定一个可能包含重复元素的整数数组 nums，返回该数组所有可能的子集（幂集）。
说明：解集不能包含重复的子集。

示例:
输入: [1,2,2]
输出:
[
  [2],
  [1],
  [1,2,2],
  [2,2],
  [1,2],
  []
]
*/

void subnums(int i, std::vector<int> nums, std::vector <int> vec, std::vector<std::vector<int>>& res)
{
	if (i > nums.size())
	{
		return;
	}

	res.push_back(vec);
	for (int j = i; j < nums.size(); j++)
	{
		if (j > i && nums[j] == nums[j - 1])
		{
			continue;
		}
		vec.push_back(nums[j]);
		subnums(j + 1, nums, vec, res);
		vec.pop_back();
	}


	return;
}

int main()
{
	int iSum = 0;
	std::vector<std::vector<int>> res;
	subnums(0, std::vector<int>{1, 2, 2}, 
		std::vector<int>{}, res);
	return 1;
}

