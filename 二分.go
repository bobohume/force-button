package main

import "fmt"

func search(nums []int, target int) int{
    l, h := 0, len(nums)-1
    for l <= h{
       // if nums[l] == target{
       //     return l
       // }else if nums[h] == target{
      //      return h
     //   }
        m := (l + h)>>1
        if nums[m] < target{
            l = m+1
        }else if nums[m] > target{
            h = m-1
        }else{
            return m
        }
    }

    return -1
}

func quickSort(nums []int, left, right int){
    i, j := left, right
    if i >= j{
        return
    }
    key := nums[i]

    for i < j{
        for ; i <j; j--{
            if nums[j] < key{
                break
            }
        }
        nums[i] = nums[j]
        for ; i <j; i++{
            if nums[i] > key{
                break
            }
        }
        nums[j] = nums[i]
    }
    nums[i] = key
    quickSort(nums, left, i)
    quickSort(nums, i+1, right)
}

func main(){
    nums := []int{1, 2, 3, 4, 6, 7, 8, 9}
    quickSort(nums, 0, len(nums)-1)
    fmt.Println(search(nums, 4))
}