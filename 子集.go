package main

import "fmt"

/*
给定一组不含重复元素的整数数组 nums，返回该数组所有可能的子集（幂集）。

说明：解集不能包含重复的子集。

示例:

输入: nums = [1,2,3]
输出:
[
  [3],
  [1],
  [2],
  [1,2,3],
  [1,3],
  [2,3],
  [1,2],
  []
]
*/

func sortAll(nums []int, pos int, res []int){
	//if pos >= len(nums){
	//	return
	//}
	fmt.Println(res)
	for i := pos; i < len(nums); i++{
		res = append(res, nums[i])
		sortAll(nums, i+1, res)
		res = append(res[:len(res)-1], res[len(res):]...)
	}
}

func main(){
	sortAll([]int{1, 2, 3}, 0, []int{})
}
