// ConsoleApplication2.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <vector>
#include <deque>
#include <cmath>
#include <algorithm>
#include <iostream>
//猜数字游戏的规则如下：
//每轮游戏，我都会从 1 到 n 随机选择一个数字。 请你猜选出的是哪个数字。
//如果你猜错了，我会告诉你，你猜测的数字比我选出的数字是大了还是小了。
//你可以通过调用一个预先定义好的接口 int guess(int num) 来获取猜测结果，返回值一共有 3 种可能的情况（ - 1，1 或 0）：
//- 1：我选出的数字比你猜的数字小 pick < num
//1：我选出的数字比你猜的数字大 pick > num
//0：我选出的数字和你猜的数字一样。恭喜！你猜对了！pick == num
//示例 1：
//输入：n = 10, pick = 6
//输出：6

//示例 2：
//输入：n = 1, pick = 1
//输出：1

//示例 3：
//输入：n = 2, pick = 1
//输出：1

//解题思路
//此题使用二分查找，将mid输入guess函数，根据返回值调整查找边界，我这里用的是【left，mid】和【mid + 1，right】
//，命中时将mid返回即可
int guess(int n, int iNum)
{
	std::vector<int> nums;
	for (auto i = 0; i <= n; i++)
	{
		nums.push_back(i);
	}

	int l = 0, h = nums.size() - 1;
	for (; l < h;)
	{
		int mid = (l + h) >> 1;
		if (nums[mid] < iNum)
		{
			l++;
		}
		else if (nums[mid] > iNum)
		{
			h--;
		}
		else{
			return mid;
		}
	}
	return -1;
}

int binary_search1(vector<int> nums, int num)
{
    int l = 0, r = nums.size() -1;
    for(;l <= r;)
    {
        int mid = (l+r) >> 1;
        if(nums[mid] < num)
        {
            l = mid + 1;
        }
        else if(nums[mid] > num)
        {
            r = mid - 1;
        }
        else
        {
            return mid;
        }
    }
    return -1;
}

int main()

int _tmain(int argc, _TCHAR* argv[])
{
	auto k = guess(2, 1);
	return 0;
}

