// ConsoleApplication2.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <vector>
#include <deque>
#include <stack>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <xfunctional>
#include <queue>
using namespace std;

/*
力扣141_环形链表时间：2021-01-18 23:19 编辑： 来源： 阅读：扫一扫，手机访问摘要：判断链表是否有环 
运用快慢指针，类似于“龟兔”，假设表中有环，那么速度快的“兔子”则首先进入环，然后在里面绕圈，
“乌龟”后入环，它们都在一个环内绕圈，由于速度不等，则一定会相遇。
*/

struct stNode
{
	stNode(int data) : data(data), pNext(NULL)
	{
	}
	int data;
	stNode* pNext;
};

bool isCycleLink(stNode* pHead)
{
	stNode* pSlow = pHead;
	stNode* pFast = pHead;
	while (pSlow != pFast)
	{
		if (pFast == NULL || pFast->pNext == NULL)
		{
			return false;
		}
		pFast = pFast->pNext->pNext;
		pSlow = pSlow->pNext;
	}
	return pSlow == pFast;
}

bool isCycleLink1(stNode* pHead)
{
	std::unordered_map<stNode*, bool> nodeMap;
	for (stNode* pNode = pHead; pNode != NULL; pNode = pNode->pNext)
	{
		if (nodeMap.count(pHead))
		{
			return false;
		}
	}
	return true;
}

int _tmain(int argc, _TCHAR* argv[])
{
	
	int kkk = 0;
}