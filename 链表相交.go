package main

import "fmt"

//160. 相交链表
//编写一个程序，找到两个单链表相交的起始节点。
//如下面的两个链表：
//在节点 c1 开始相交。

//示例 1：
//输入：intersectVal = 8, listA = [4,1,8,4,5], listB = [5,0,1,8,4,5], skipA = 2, skipB = 3
//输出：Reference of the node with value = 8
//输入解释：相交节点的值为 8 （注意，如果两个链表相交则不能为 0）。从各自的表头开始算起，链表 A 为 [4,1,8,4,5]，
//链表 B 为 [5,0,1,8,4,5]。在 A 中，相交节点前有 2 个节点；在 B 中，相交节点前有 3 个节点。

//示例 2：
//输入：intersectVal = 2, listA = [0,9,1,2,4], listB = [3,2,4], skipA = 3, skipB = 1
//输出：Reference of the node with value = 2
//输入解释：相交节点的值为 2 （注意，如果两个链表相交则不能为 0）。从各自的表头开始算起，链表 A 为 [0,9,1,2,4]，
//链表 B 为 [3,2,4]。在 A 中，相交节点前有 3 个节点；在 B 中，相交节点前有 1 个节点。

//示例 3：
//输入：intersectVal = 0, listA = [2,6,4], listB = [1,5], skipA = 3, skipB = 2
//输出：null
//输入解释：从各自的表头开始算起，链表 A 为 [2,6,4]，链表 B 为 [1,5]。由于这两个链表不相交，
//所以 intersectVal 必须为 0，而 skipA 和 skipB 可以是任意值。
//解释：这两个链表不相交，因此返回 null。
//注意：
//如果两个链表没有交点，返回 null.
//在返回结果后，两个链表仍须保持原有的结构。
//可假定整个链表结构中没有循环。
//程序尽量满足 O(n) 时间复杂度，且仅用 O(1) 内存。
//思路一：双重循环暴力法
//时间复杂度为O(n*m),不符合题目对时间复杂度的要求
//思路二：hashSet+遍历
//时间复杂度为O(n), 但是空间复杂度为也为O(n), 不符合题目对空间复杂度的要求
//思路三：双指针法
//两个指针p1, p2分别指向两个链表，对两个链表同时进行遍历，如果p1或者p2为空则指向另一个链表，继续遍历
//如果两个链表存在公共结点，那么会在while循环的p1 != p1这个条件判断退出循环
//如果两个链表不存在公共结点，因为两个链表的尾部都接上了对方的链表，所以修改后的两链表长度是一样的，所以p1和p2会同时到达链表尾部，同时指向null, 条件判断时p1 == p2，从而退出循环
type(
	ListNode struct {
		data int
		next *ListNode
		pre *ListNode
	}
)

func listInterest(pListA *ListNode, pListB *ListNode){
	for ; pListA != nil && pListA.next != nil;pListA = pListA.next{
		pListA.next.pre = pListA
	}
	pA := pListA
	fmt.Println(pA)
}

func insertList(data int, pNode *ListNode)*ListNode{
	pNode1 :=  &ListNode{data, pNode, nil}
	return pNode1
}

func getLink(head *ListNode)*ListNode{
	if head == nil || head.next == nil {
		return head
	}

	p := getLink(head.next)
	head.next.next = head
	head.next = nil
	return  p
}

func main(){
	pA := &ListNode{1, nil, nil}
	pA = insertList(2, pA)
	pA = insertList(3, pA)
	//listInterest(pA, pA)

	pA = getLink(pA)
	kkk := 0
	kkk++
}