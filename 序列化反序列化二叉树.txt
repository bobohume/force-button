// ConsoleApplication2.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <vector>
#include <deque>
#include <stack>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <xfunctional>
#include <queue>
using namespace std;
/*
序列化和反序列化二叉树
      1
   3    2
  4 5  6 7 
*/

struct BTree
{
	int data;
	BTree* lchild;
	BTree* rchild;
	BTree(int data) : data(data),
		lchild(NULL), rchild(NULL)
	{
	}
};

std::deque<int> g_nums;

void marshal(BTree* root)
{
	if (!root)
	{
		g_nums.push_back(-1);
		return;
	}

	g_nums.push_back(root->data);
	marshal(root->lchild);
	marshal(root->rchild);
}


BTree* unmarshal()
{
	if (!g_nums.empty())
	{	
		int data = g_nums.front();
		g_nums.pop_front();
		if (data == -1)
		{
			return NULL;
		}
		BTree* tree = new BTree(data);
		tree->lchild = unmarshal();
		tree->rchild = unmarshal();
		return tree;
	}
	return NULL;
}



int _tmain(int argc, _TCHAR* argv[])
{
	auto tr1 = new BTree(1);
	auto tr2 = new BTree(2);
	auto tr3 = new BTree(3);
	tr1->lchild = tr3;
	tr1->rchild = tr2;

	auto tr4 = new BTree(4);
	auto tr5 = new BTree(5);
	tr3->lchild = tr4;
	tr3->rchild = tr5;

	auto tr6 = new BTree(6);
	auto tr7 = new BTree(7);
	tr2->lchild = tr6;
	tr2->rchild = tr7;
	marshal(tr1);
	auto tr = unmarshal();
	int kkk = 0;
}
