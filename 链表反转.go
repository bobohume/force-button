package main

import "fmt"

type(
	ListNode struct {
		data int
		next *ListNode
	}
)

func InsertListNode(head *ListNode, data int) *ListNode{
	node := &ListNode{data, head}
	return node
}

func ReverseListNode(head *ListNode) *ListNode{
	if head == nil || head.next == nil{
		return head
	}

	p := ReverseListNode(head.next)
	head.next.next = head
	head.next = nil
	return p
}

func main(){
	head := &ListNode{1, nil}
	head = InsertListNode(head, 2)
	head = InsertListNode(head, 3)
	head = InsertListNode(head, 4)
	head = InsertListNode(head, 5)
	head = ReverseListNode(head)
	fmt.Println(head)
}
