package main

import "fmt"

type (
	TreeArray struct {
		Len   int
		Arr   []int
		m_Pid map[int]int
	}
)

func (this *TreeArray) Lowbit(x int) int {
	return x & (-x)
}

func (this *TreeArray) Sum(i int) int {
	if i >= this.Len {
		return 0
	}

	s := 0
	for ; i > 0; i -= this.Lowbit(i) {
		s += this.Arr[i]
	}
	return s
}

func (this *TreeArray) Add(i, val int) {
	for ; i > 0 && i < this.Len; i += this.Lowbit(i) {
		this.Arr[i] += val
	}
}

func (this *TreeArray) Build(arr []int) {
	this.Len = len(arr)
	this.Arr = make([]int, this.Len)
	for i, v := range arr {
		this.Arr[i] += v
		j := i + this.Lowbit(i)
		if j < this.Len {
			this.Arr[j] += this.Arr[i]
		}
	}
}

func (this *TreeArray) AddRank(pid, rank int) {
	bEx, old_rank = this.m_Pid[pid]
	if bEx {

	}
}

func main() {
	t := TreeArray{}
	t.Build([]int{1, 1, 1, 1, 1, 2, 1, 2})
	fmt.Println(t.Arr)
	t.Add(7, -1)
	fmt.Println(t.Arr)
	fmt.Println(t.Sum(t.Len-1) - t.Sum(6) + 1)
}
