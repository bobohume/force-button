package main

import "math"

//给定一个整数数组 nums ，找出一个序列中乘积最大的连续子序列（该序列至少包含一个数）。
//示例 1:
//输入: [2,3,-2,4]
//输出: 6
//解释: 子数组 [2,3] 有最大乘积 6。
//解题思路:
//1.当出现负数的时候，最大值和最小值要进行交换，这样保证最大的还是最大的。最小的还是最小的。
//2.记录最大值

func  maxProduct(nums []int) int{
	maxDp := make([]int, len(nums))
	minDp := make([]int, len(nums))
	maxDp[0] = nums[0]
	minDp[0] = nums[0]

	maxp := 0
	for i := 1; i < len(nums); i++{
		maxDp[i] = int(math.Max(math.Max(float64(maxDp[i-1] * nums[i]), float64(nums[i])), float64(minDp[i-1] * nums[i])))
		minDp[i] = int(math.Min(math.Min(float64(minDp[i-1] * nums[i]), float64(nums[i])), float64(maxDp[i-1] * nums[i])))
		maxp = int(math.Max(float64(maxp), float64(maxDp[i])))
	}
	return maxp
}

func main(){
	maxProduct([]int{2, 3, -2, -4, 4})
}
