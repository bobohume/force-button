# 看一段hotfix例子

package main

func a() int { return 1}

func b() int { return 2}

func main() {

  patch(a, b)

  fmt.Println(a())
    
}