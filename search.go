package main

import (
	"fmt"
)

func quicksort(arr []int, left, right int){
	i, j := left, right
	if i >= j{
		return
	}

	key := arr[i]
	for i < j{
		for ; i < j; j--{
			if arr[j] < key{
				break
			}
		}
		arr[i] = arr[j]
		for ; i < j;i++{
			if arr[i] > key{
				break
			}
		}
		arr[j] = arr[i]
	}
	arr[i] = key
	quicksort(arr, left, i)
	quicksort(arr, i+1, right)
}

func search(arr []int, val int) int{
	l, h := 0, len(arr)-1
	for l <= h{
		m := (l + h) >> 1
		if arr[m] < val{
			l = m + 1
		}else if arr[m] > val{
			h = m - 1
		}else{
			return m
		}
	}
	return -1
}

func main(){
	nums := []int{1, 3, 5, 7, 2, 4, 9, 10}
	quicksort(nums, 0, len(nums)-1)
	fmt.Println(nums)
	fmt.Println(search(nums, 11))
}