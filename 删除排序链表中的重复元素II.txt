// test.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <vector>
#include <deque>
#include <stack>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <queue>
using namespace std;

/*
给定一个排序链表，删除所有重复的元素，使得每个元素只出现一次。

示例 1:

输入: 1->1->2
输出: 1->2
示例 2:

输入: 1->1->2->3->3
输出: 1->2->3
*/

struct stNode 
{
	int data;
	stNode* next;
	stNode(int data) :data(data), next(NULL)
	{
	}
};

void deleSameNode(stNode* head)
{
	stNode* pNode = head;
	for (; head != NULL; head = head->next)
	{
		if (head->data != pNode->data)
		{
			pNode->next = head;
			pNode = pNode->next;
		}
	}
}

stNode* inserNode(stNode* head, int data)
{
	stNode* node = new stNode(data);
	head->next = node;
	return node;
}

int main()
{
	stNode* head = new stNode(1);
	auto pNewNode = inserNode(head, 1);
	pNewNode = inserNode(pNewNode, 2);
	pNewNode = inserNode(pNewNode, 3);
	pNewNode = inserNode(pNewNode, 3);
	deleSameNode(head);
}