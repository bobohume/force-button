package  main

import (
	"fmt"
	"math"
)

/*
容器最大盛水量
Container With Most Water
给定n个非负整数a1，a2，…，an，其中每个表示坐标（i，ai）处的点。
绘制n条垂直线，使得线i的两个端点在（i，ai）和（i，0）处。
找到两条线，它们与x轴一起形成一个容器，使得容器含有最多的水。
注意：您不得倾斜容器，n至少为2。
*/

func max(a, b int) int{
	return int(math.Max(float64(a), float64(b)))
}

//水桶盛水
func maxTongChengShui(height []int) int{
	l, r := 0, len(height)-1
	maxArea := 0
	for l < r{
		if height[l] < height[r]{
			area := height[l] * (r - l)
			maxArea = max(maxArea, area)
			l++
		}else{
			area := height[r] * (r - l)
			maxArea = max(maxArea, area)
			r--
		}
	}
	return maxArea
}

func main(){
	fmt.Println(maxTongChengShui([]int{1, 8, 6,2,5,4,8,3,7}))
}