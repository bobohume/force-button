package main

import "fmt"
//给定一个字符串，请你找出其中不含有重复字符的 最长子串 的长度。
//
//示例 1:
//输入: "abcabcbb"
//输出: 3
//解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
//示例 2:
//
//输入: "bbbbb"
//输出: 1
//解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
//示例 3:
//
//输入: "pwwkew"
//输出: 3
//解释: 因为无重复字符的最长子串是 "wke"，所以其长度为 3。
//     请注意，你的答案必须是 子串 的长度，"pwke" 是一个子序列，不是子串。

func maxNoreplaceStr(str string)string{
	resut := ""
	for i := 0; i <len(str); i++{
		sMap := map[byte] bool{str[i]:true}
		for j := i+1; j < len(str); j++{
			_, bEx := sMap[str[j]]
			if !bEx{
				sMap[str[j]] = true
				continue
			}else{
				if len(sMap) > len(resut){
					resut = ""
					for i, _ := range sMap{
						resut += string(i)
					}
					break
				}
			}
		}
	}
	return resut
}

func main(){
	fmt.Println(maxNoreplaceStr("pwwwkeafw"))
}