package main

type(
	listNode struct {
		data int
		next *listNode
	}
)

//给定一个链表，删除链表的倒数第 n 个节点并返回头结点。
//
//例如，
//给定一个链表: 1->2->3->4->5, 并且 n = 2.
//
//当删除了倒数第二个节点后链表变成了 1->2->3->5.

//双指针
func deleteNodeN(l *listNode, n int){
	first, seconde := l, l
	for i := 0; first.next != nil && i <= n; i++{
		first = first.next
	}

	for first != nil{
		first = first.next
		seconde = seconde.next
	}
	seconde.next = seconde.next.next
}

func main(){
	l := &listNode{}
	l1 := l
	for i := 1; i <= 5; i++{
		l1.next =  &listNode{i, nil}
		l1 = l1.next
	}

	deleteNodeN(l, 2)
}